﻿using UnityEngine;

namespace Assets.Scripts
{
    public class card_def : ScriptableObject
    {
        /// <summary>
        /// image of card
        /// </summary>
        public Sprite Sprite;
    }
}