﻿using UnityEngine;

namespace Assets.Scripts
{
    public abstract class data_context_widget : MonoBehaviour
    {
        /// <summary>
        /// cached transform (to avoid calling transform which is wrapper around GetComponent)
        /// </summary>
        public Transform Transform;

        /// <summary>
        /// data to show
        /// </summary>
        [SerializeField]
        private Object _dataContext;

        public Object data_context
        {
            get {
                return _dataContext;
            }
            set {
                if (_dataContext == value) {
                    return;
                }

                var oldValue = _dataContext;
                _dataContext = value;

                OnDataContextChanged(oldValue);
            }
        }

        protected abstract void OnDataContextChanged(Object oldValue);
    }
}