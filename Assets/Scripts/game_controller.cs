﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class game_controller : MonoBehaviour
    {
        /// <summary>
        /// list of all available cards in game
        /// </summary>
        public List<card_def> cards;
        /// <summary>
        /// cards that are not dealed
        /// </summary>
        private List<card_def> _cards_stock; 
        /// <summary>
        /// dealed cards (cards in list view)
        /// </summary>
        private List<card_def> _cards_in_hand;

        /// <summary>
        /// link to list view. User's hand
        /// </summary>
        public homework_list_view list_view;
        /// <summary>
        /// Label when status will be shown (cards count in hand, in stock)
        /// </summary>
        public UILabel status_label;
        /// <summary>
        /// if we have wornings waiting to show, here warning will be show
        /// </summary>
        public UILabel warning_label;

        /// <summary>
        /// list of warnings that are waiting to show
        /// </summary>
        private List<string> _warning_messages;  

        private void Awake() {
            cards = cards ?? new List<card_def>();
            _cards_in_hand = new List<card_def>();
            _cards_stock= new List<card_def>();
            _warning_messages = new List<string>();
        }

        private void Start() {
            //fill cards stock
            //start coroutine that will check warning messages waiting to show and show them
            //deal the cards
            
            _cards_stock = cards.ToList();
            StartCoroutine(ShowWarningMessagesCoroutine());
        
            DealTheCards();
        }

        public void DealTheCards() {
            
            //clear hand
            //send cards to listview
            //remember what cards we have dealed (all of them)

            if (_cards_in_hand.Count == cards.Count) {
                ShowWarningMessage("Cards are dealed!");
                return;
            }

            if (_cards_in_hand.Count > 0) {
                ClearHand();
            }

            _cards_in_hand = cards.ToList();
            _cards_stock.Clear();

            list_view.AddDataItems(_cards_in_hand);
        }

        public void ClearHand() {
            
            //clear listview data
            //clear cards in hand
            //fill the stock with cards

            if (_cards_in_hand.Count == 0) {
                ShowWarningMessage("Hand is clear!");
                return;
            }

            list_view.ClearData();

            _cards_stock = cards.ToList();
            _cards_in_hand.Clear();
        }

        public void DealRandomCard() {
            
            //check is there anything to deal. if no, show warning
            //get random number for card, extract it and deal
            //remember what card we just dealed

            if (_cards_stock.Count == 0) {
                ShowWarningMessage("All cards are dealed!");
                return;
            }

            var index = Random.Range(0, _cards_stock.Count);
            var card = _cards_stock[index];
            
            _cards_stock.RemoveAt(index);
            _cards_in_hand.Add(card);

            list_view.AddDataItem(card);
        }

        public void RemoveRandomCard() {
            
            //check is there any card to move from hand to stock
            //if any, take it from hand, move to stock. say scrollview to remove that card
            //if no cards in hand, show warning

            if (_cards_in_hand.Count == 0) {
                ShowWarningMessage("No cards in hand...");
                return;
            }

            var index = Random.Range(0, _cards_in_hand.Count);
            var card = _cards_in_hand[index];
            
            _cards_in_hand.RemoveAt(index);
            _cards_stock.Add(card);

            list_view.RemoveItem(card);
        }

        private void Update() {
            
            //collect data to status label, show it
            
            var str = new StringBuilder();
            str.AppendFormat("Cards in stock: {0}", _cards_stock.Count);
            str.AppendLine();
            str.AppendFormat("Cards in hand: {0}", _cards_in_hand.Count);
            str.AppendLine();

            status_label.text = str.ToString();
        }

        private void ShowWarningMessage(string message) {
            //just add message to warnings queue

            _warning_messages.Add(message);
        }

        private IEnumerator ShowWarningMessagesCoroutine() {
            
            //this coroutine works forever, checks is there any warning message waiting to show
            //and if any, extract it from queue and show
            
            while (true) {
                if (_warning_messages.Count == 0) {
                    yield return new WaitForSeconds(0.2f);
                    continue;
                }

                if (_warning_messages.Count > 0) {
                    var message = _warning_messages[_warning_messages.Count - 1];
                    _warning_messages.RemoveAt(_warning_messages.Count - 1);

                    warning_label.text = message;
                    warning_label.gameObject.SetActive(true);

                    yield return new WaitForSeconds(2f);

                    warning_label.text = string.Empty;
                    warning_label.gameObject.SetActive(false);
                }
            }
        }
    }
}