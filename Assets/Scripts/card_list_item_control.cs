﻿using UnityEngine;

namespace Assets.Scripts
{
    public class card_list_item_control : data_context_widget
    {
        /// <summary>
        /// widget to show card's image
        /// </summary>
        public UI2DSprite sprite_container;
        /// <summary>
        /// root of the widget. needs to calculate size
        /// </summary>
        public UIWidget root_widget;

        protected override void OnDataContextChanged(Object oldValue) {
            //check is data a card? if true, show card's image. else - hide sprite

            var config = data_context as card_def;

            if (config != null) {
                sprite_container.sprite2D = config.Sprite;
            }
            else {
                sprite_container.sprite2D = null;
            }
        }
    }
}