﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class homework_list_view : MonoBehaviour
    {
        /// <summary>
        /// shortlink to a scrollview widget. 
        /// </summary>
        private UIScrollView _scroll_view;
        /// <summary>
        /// my panel (shortlink)
        /// </summary>
        private UIPanel _panel;
        /// <summary>
        /// container where shown item controls are placed
        /// </summary>
        private UITable _items_container;
        /// <summary>
        /// link to a prefab of item control
        /// </summary>
        public data_context_widget item_prefab;
        /// <summary>
        /// list of items that are shown
        /// </summary>
        public List<data_context_widget> shown_items;
        /// <summary>
        /// pool of items to show
        /// </summary>
        public List<data_context_widget> items_pool;
        /// <summary>
        /// data to show 
        /// </summary>
        public List<card_def> items_data;

        /// <summary>
        /// Cached transform
        /// </summary>
        private Transform _transform;
        /// <summary>
        /// cached transform of items_container
        /// </summary>
        private Transform _items_container_transform;

        private Vector2 _size;
        /// <summary>
        /// size of the panel. Changes with a screen resolution
        /// </summary>
        public Vector2 size
        {
            get {
                return _size;
            }
            set {
                if (_size == value) {
                    return;
                }

                _size = value;

                OnSizeChanged();
            }
        }

        [Header("System data")]
        [SerializeField]
        private float _item_width;
        /// <summary>
        /// item control's width. Need to calculate how much controls can be show at the same time
        /// </summary>
        public float item_width
        {
            get {
                if (_item_width <= 0) {
                    return 0;
                }

                return _item_width + _items_container.padding.x*2;
            }
            set {
                _item_width = value;
            }
        }

        /// <summary>
        /// How much item controls must be added to viewSize/item_width in calculating max_items_count
        /// </summary>
        public int additional_item_controls_count = 4;

        [SerializeField]
        private int _max_items_count;
        /// <summary>
        /// count of items that can be shown at the same time
        /// </summary>
        public int max_items_count
        {
            get {
                _max_items_count = 0;

                if (item_width > 0) {
                    var viewSize = _panel.GetViewSize().x;
                    _max_items_count = Mathf.CeilToInt(viewSize / item_width) + additional_item_controls_count;
                }

                return _max_items_count;
            }
        }

        [SerializeField]
        private bool _is_oversize;
        /// <summary>
        /// when this value is true that means we have more items than can show at the same time. so carouselle behaviour should be activated
        /// </summary>
        public bool is_oversize
        {
            get {
                _is_oversize = max_items_count > 0 && shown_items.Count >= max_items_count;
                return _is_oversize;
            }
        }

        [SerializeField]
        private int _first_shown_item_data_index;
        /// <summary>
        /// calculated value for index of data to be shown in first visible item control
        /// </summary>
        public int first_shown_item_data_index
        {
            get {
                return _first_shown_item_data_index;
            }
            set {
                value = value % items_data.Count;
                if (value < 0) {
                    value = items_data.Count + value;
                }

                if (_first_shown_item_data_index == value) {
                    return;
                }

                _first_shown_item_data_index = value;

                _is_need_to_reassign_data_context = true;
            }
        }

        /// <summary>
        /// when this value is true, that means we should update data_context for all visible item controls
        /// </summary>
        private bool _is_need_to_reassign_data_context;

        private void Awake() {
            
            //create all the lists, get all needed components
            //save my current size (for cases when screen resolution will change)

            _scroll_view = GetComponent<UIScrollView>();
            _items_container = _scroll_view.GetComponentInChildren<UITable>();
            _items_container_transform = _items_container.transform;
            _panel = GetComponent<UIPanel>();
            _transform = transform;

            items_pool = items_pool ?? new List<data_context_widget>();
            shown_items = shown_items ?? new List<data_context_widget>();
            items_data = items_data ?? new List<card_def>();

            _size = new Vector2(_panel.width, _panel.height);
        }

        private void Update() {
            //check screen resolution change
            //recalculate correct clip offset and position of items container
            size = new Vector2(_panel.width, _panel.height);

            CalcCorrectClipAndPos();
            ReassignDataContext();
        }

        private void CalcCorrectClipAndPos() {
            
            //check is count of data items is more or less than we can show at the same time
            //if not, there's nothing to do here
            //recalculate position and clipOffset to correct showing items
            //recalculate new data-index for first visible item control

            if (!is_oversize) {
                //items count is less than view area can contain. In this case listView works as designed by default, not as carouselle
                return;
            }

            var offset = -_transform.localPosition.x;
            var sign = Math.Sign(offset);
            var itemsSkipped = Mathf.FloorToInt(Math.Abs(offset)/item_width);
            itemsSkipped *= sign;

            if (itemsSkipped != 0) {
                var translateAmount = itemsSkipped*item_width;

                _panel.clipOffset -= new Vector2(translateAmount, 0);
                _transform.localPosition += new Vector3(translateAmount, 0, 0);
            }

            first_shown_item_data_index += itemsSkipped;
        }

        private void ReassignDataContext() {
            //go through all the visible item controls and set them correct data_context
            //if there's excess item controls, release them
            
            if (!_is_need_to_reassign_data_context) {
                return;
            }

            _is_need_to_reassign_data_context = false;

            var items_to_hide = new List<data_context_widget>();

            for (int i = 0; i < shown_items.Count; i++) {
                var item = shown_items[i];
                if (items_data.Count == 0) {
                    items_to_hide.Add(item);
                    continue;
                }

                var dataIndex = (first_shown_item_data_index + i)%items_data.Count;
                if (dataIndex < 0) {
                    dataIndex = items_data.Count + dataIndex;
                }

                if (dataIndex >= items_data.Count) {
                    items_to_hide.Add(item);
                    continue;
                }

                var data = items_data[dataIndex];
                item.data_context = data;
            }

            for (int i = 0; i < items_to_hide.Count; i++)
            {
                var control = items_to_hide[i];
                ReleaseItemControl(control);
            }
        }

        public void ClearData() {
            //clear data (as long as we have nothing to show anymore)
            //clear controls that show that data

            items_data.Clear();
            ClearShownControls();
        }

        private void ClearShownControls() {
           
            //go through visible controls and release them
            
            for (int i = 0; i < shown_items.Count; i++) {
                var control = shown_items[i];
                ReleaseItemControl(control);
            }

            shown_items.Clear();
        
            _items_container.Reposition();
        }

        private data_context_widget GetItemControl() {
            
            //check is any item control in pool? If yes, get it from pool
            //if pool is empty, create new instance of item control
            
            data_context_widget result;

            if (items_pool.Count > 0) {
                result = items_pool[items_pool.Count - 1];
                items_pool.RemoveAt(items_pool.Count - 1);

                result.gameObject.SetActive(true);
            }
            else {
                result = CreateItemControl();
            }

            return result;
        }

        private data_context_widget CreateItemControl() {
            //create item control. We have link to prefab, so it's quite simple
            
            return Instantiate(item_prefab);
        }

        private void ReleaseItemControl(data_context_widget control) {
            
            //item control is not required anymore, so let's send it to the pool (when time'll come, control will be extracted from pool) 
            //just to avoid creating/destroing controls every time

            control.data_context = null;
            control.Transform.SetParent(null);
            control.gameObject.SetActive(false);
            
            items_pool.Add(control);
        }

        public void AddDataItems(IEnumerable<card_def> dataItems) {
            
            //add data to data-array
            //call function to update item controls (create more if we have less than necessary)

            items_data.AddRange(dataItems);
            CheckItemsCount();
        }

        public void AddDataItem(card_def dataItem) {

            //add data item into data-array
            //call function to decide what to do (create new control for data, or may be data isn't shown yet?)
            
            if (dataItem == null) {
                return;
            }

            items_data.Add(dataItem);
            OnDataItemAdded(dataItem);

            _is_need_to_reassign_data_context = true;
        }

        private void OnDataItemAdded(card_def dataItem) {
            
            //check is we have enough item controls created to fill visible area? 
            //if not, create new control

            if (max_items_count <= 0 || shown_items.Count < max_items_count) {
                AddItemControl(dataItem);
            }
        }

        private void AddItemControl(card_def dataItem) {
            
            //request control (it will be extracted from pool or create if pool is empty)
            //insert control into listview
            //set control's data_context

            //if we don't know size of the item control yet, check it. we can't just take size from prefab, 
            //'cause prefab is'nt parented to listview and it's size may be incorrect (since size must be calculated relative to parent)

            var control = GetItemControl();
            control.Transform.SetParent(_items_container_transform, false);
            control.data_context = dataItem;

            if (item_width <= 0) {
                var controlBounds = NGUIMath.CalculateRelativeWidgetBounds(_transform, control.Transform);
                item_width = controlBounds.size.x;
            }

            shown_items.Add(control);

            _items_container.Reposition();
        }

        private void Start() {
            
            //call check_items_count for case when listView created with data-array filled. In this case we need to show it

            CheckItemsCount();
        }

        private void CheckItemsCount() {
            //how much items do we need to fit? 
            //how much do we have? 
            //what's the delta of them
            //if we have data items enough to fill area, fill it

            if (max_items_count == 0 && items_data.Count > 0) //we have no any item added yet... but have data for items!
            {
                //need to add item to determine what is the size of the item
                AddItemControl(items_data[0]);
            }

            var needControls = Math.Min(max_items_count, items_data.Count);
            var haveControls = shown_items.Count;

            var delta = needControls - haveControls;

            //if no changes required, there's nothing to do here...
            if (delta == 0) {
                return;
            }

            //add controls
            for (int i = 0; i < delta; i++) {
                if (items_data.Count <= shown_items.Count) {
                    break;
                }

                AddItemControl(null);
            }

            //remove controls
            for (int i = 0; i < -delta; i++) {
                if (shown_items.Count == 0) {
                    break;
                }

                var control = shown_items[shown_items.Count - 1];
                shown_items.RemoveAt(shown_items.Count - 1);

                ReleaseItemControl(control);
            }

            //mark me as dataContext-unassigned for update items data
            _is_need_to_reassign_data_context = true;
            _items_container.Reposition();
        }

        private void OnSizeChanged() {
            //when size changed, we have to recalculate visible area and how much controls will be shown at the same time now. 
            //may be some controls must be released? or created? 
            //and if we have data to show, show that data
            
            CheckItemsCount();
        }

        public void RemoveItem(card_def dataItem)
        {
            //remove dataitem from data-array
            //check items count (may be it's time to remove control? 
            //or if this dataItem was visible, reassign it's control's data_context to some other data?)

            items_data.Remove(dataItem);

            CheckItemsCount();
            _is_need_to_reassign_data_context = true;
        }
    }
}